FROM registry.gitlab.com/avisi/base/java:8

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ARG WKHTMLTOPDF_VERSION=0.12
ARG WKHTMLTOPDF_VERSION_FULL=0.12.5
ARG WKHTMLTOPDF_SHA256=ac4f909b836fa1fc0188d19a1ab844910f91612e9ccefcb5298aa955a058ffe4

USER root

RUN yum install -y openssl libpng libjpeg which \
    icu libX11 libXext libXrender \
    xorg-x11-fonts-Type1 xorg-x11-fonts-75dpi \
    && yum clean all

RUN curl -L -O https://downloads.wkhtmltopdf.org/${WKHTMLTOPDF_VERSION}/${WKHTMLTOPDF_VERSION_FULL}/wkhtmltox-0.12.5-1.centos7.x86_64.rpm \
    && echo "${WKHTMLTOPDF_SHA256} wkhtmltox-${WKHTMLTOPDF_VERSION_FULL}-1.centos7.x86_64.rpm" | sha256sum -c \
    && rpm -Uvh wkhtmltox-${WKHTMLTOPDF_VERSION_FULL}-1.centos7.x86_64.rpm \
    && rm -rf wkhtmltox-${WKHTMLTOPDF_VERSION_FULL}-1.centos7.x86_64.rpm

USER avisi
