# Avisi wkhtmltopdf

This repository contains the Avisi PEC wkhtmltopdf image.

[![pipeline status](https://gitlab.com/avisi/pec/wkhtmltopdf/badges/master/pipeline.svg)](https://gitlab.com/avisi/pec/wkhtmltopdf/commits/master)

---

### Tags

| Tag | Notes |
|-----|-------|
| `0.12.5` | wkhtmltopdf version 0.12.5 |

## Usage

```dockerfile
FROM registry.gitlab.com/avisi/pec/wkhtmltopdf:0.12.5

```

## Issues

All issues should be reported in the [Gitlab issue tracker](https://gitlab.com/avisi/pec/wkhtmltopdf/issues).
